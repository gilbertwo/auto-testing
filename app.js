const Nightmare = require('nightmare');

const nightmare = Nightmare({show: true, width: 1500, height: 900});
const settings = {
    auth: {
        url: `https://app.detailonline.com/`,
        email: `admin@detailonline.tech`,
        password: `adminpassword`
    }
};

(async () => {
    try {
        await nightmare
            .goto(settings.auth.url)

            // click login button
            // wait until the UI is loaded
            .wait(`input[type=email]`)
            .type(`input[type=email]`, settings.auth.email)
            .type(`input[type=password]`, settings.auth.password)
            .click(`.button.full`)

            // click group menu button
            // wait until the group button is loaded
            .wait(`.group-dropdown-wrapper button[name=menu]`)
            .wait(1000)
            .click(`.group-dropdown-wrapper button[name=menu]`)

            // click or select group from dropdown
            .wait(1000)
            .click(`.overview-wrapper .dropdown-menu .group-row  div[title='Selenium Demo All Retailers (QA Test)']`)

            // play page
            // play app sample for script injection in the browser------------!!
            .wait(3000)
            .evaluate(async () => {
                let els = document.querySelectorAll('button, label, span, input, .text-title-gray-B, .text-title-gray-A, li, img, svg, .dropdown, .dropdown-toggle, .donut-visualization-label, .number-tracked-sites, .number-monitored-products')
                let styleEl = document.createElement('style')
                let promptDiv = document.createElement('div')
                let animationEffect = `
                @keyframes runningElement {
                    0%   {
                        top: 0%;
                        left: 0%;
                        transform: rotate(90deg);
                    }
                    20%  {
                        top: 0%;
                        left: 50%;
                        transform: rotate(180deg);
                    }
                    40%  {
                        top: 50%;
                        left: 50%;
                        transform: rotate(260deg);
                    }
                    60% {
                        top: 50%;
                        left: 10%;
                        transform: rotate(310deg);
                    }
                    80% {
                        top: 20%;
                        left: 20%;
                        transform: rotate(0deg);
                    }
                    @keyframes vibrateEl {
                        0%   {
                            top: 3px;
                            left: 1px;
                        }
                        50% {
                            top: 0px;
                            left: 0px;
                        }
                }
                `

                document.head.append(styleEl)
                styleEl.innerHTML = animationEffect

                els.forEach((el) => {
                    let randTime = Math.round(Math.random() * 10000)
                    setTimeout(() => {
                        // animation-name: example;
                        // animation-duration: 4s;
                        // animation-iteration-count: infinite;
                        el.style.position = 'relative'
                        el.style.top = '0%'
                        el.style.left = '0%'
                        el.style.animationName = 'runningElement'
                        el.style.animationDuration = '5s'
                        el.style.animationIterationCount = 'infinite'
                    }, randTime)
                })

                // prompt
                document.body.append(promptDiv)
                promptDiv.innerHTML = 'Alert! system is vulnerable! <a href="mailto:gilbert.cuerbo@detailonline.tech">Click Here! </a>'
                promptDiv.style.padding = '50px'
                promptDiv.style.fontSize = '16pt'
                promptDiv.style.zIndex = '1000'
                promptDiv.style.color = 'red'
                promptDiv.style.background = 'white'
                promptDiv.style.position = 'absolute'
                promptDiv.style.zIndex = '1000'
                promptDiv.style.boxShadow = '1px 0px 3px 0px rgba(0, 0, 0, 0.4)'
                promptDiv.style.left = '28%'
                promptDiv.style.top = '20%'
                promptDiv.style.opacity = 0
                promptDiv.style.transition = 'opacity .5s'

                window.onclick = () => {
                    promptDiv.style.opacity = 1
                    setTimeout(() => {
                        promptDiv.style.opacity = 0
                    }, 5000)
                }
            })


    } catch (err) {
        console.log('yawa naay error: ', err)
    }
})()