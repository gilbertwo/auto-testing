# auto-testing
automatic testing for QA



## dependencies
 - node.js [javascript engine]
    -- link => https://nodejs.org/dist/v10.16.3/node-v10.16.3-x64.msi

 - repository
    -- https => https://gilbertwo@bitbucket.org/gilbertwo/auto-testing.git




## How to setup
 [step 1]
        => install node.js on your machine just got to the link indicated above.

 [step 2]
        => test node.js installation
        => open cmd then try: node -v
        => and npm -v

 [step 3]
        => on your cmd, type: cd [directory_where_you_want_the_testing_app_to_be_installed]
        => clone the repository by typing: git clone [repo_link]
        => after cloning, type: cd auto-testing
        => then type: npm install




## How to run the app
 [step 1]
        => open cmd
        => then type: cd [directory_where_you_want_the_testing_app_to_be_installed]

 [step 2]
        => run the app, type: npm test




NOTE! if you got error just contact frontend team or email gilbert.cuerbo@detailonline.tech